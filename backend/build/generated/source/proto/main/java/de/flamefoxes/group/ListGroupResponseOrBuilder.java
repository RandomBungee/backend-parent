// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: group/group_service.proto

package de.flamefoxes.group;

public interface ListGroupResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:group.ListGroupResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>repeated .group.Group group = 1;</code>
   */
  java.util.List<de.flamefoxes.group.Group> 
      getGroupList();
  /**
   * <code>repeated .group.Group group = 1;</code>
   */
  de.flamefoxes.group.Group getGroup(int index);
  /**
   * <code>repeated .group.Group group = 1;</code>
   */
  int getGroupCount();
  /**
   * <code>repeated .group.Group group = 1;</code>
   */
  java.util.List<? extends de.flamefoxes.group.GroupOrBuilder> 
      getGroupOrBuilderList();
  /**
   * <code>repeated .group.Group group = 1;</code>
   */
  de.flamefoxes.group.GroupOrBuilder getGroupOrBuilder(
      int index);
}
