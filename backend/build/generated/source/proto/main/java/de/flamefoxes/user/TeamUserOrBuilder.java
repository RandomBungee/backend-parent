// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: user/team_user.proto

package de.flamefoxes.user;

public interface TeamUserOrBuilder extends
    // @@protoc_insertion_point(interface_extends:user.TeamUser)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>optional int64 total_bans = 1;</code>
   */
  long getTotalBans();

  /**
   * <code>optional int64 total_mutes = 2;</code>
   */
  long getTotalMutes();

  /**
   * <code>optional int64 total_reports = 3;</code>
   */
  long getTotalReports();

  /**
   * <code>optional int64 total_report_accept = 4;</code>
   */
  long getTotalReportAccept();

  /**
   * <code>optional int64 total_report_denied = 5;</code>
   */
  long getTotalReportDenied();

  /**
   * <code>optional int64 total_wrongbans = 6;</code>
   */
  long getTotalWrongbans();

  /**
   * <code>optional int64 total_wrongmutes = 7;</code>
   */
  long getTotalWrongmutes();

  /**
   * <code>optional int32 admission = 8;</code>
   */
  int getAdmission();

  /**
   * <code>optional string unique_id = 9;</code>
   */
  java.lang.String getUniqueId();
  /**
   * <code>optional string unique_id = 9;</code>
   */
  com.google.protobuf.ByteString
      getUniqueIdBytes();
}
