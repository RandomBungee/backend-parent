// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: punishment/mute_service.proto

package de.flamefoxes.punishment;

public interface DeleteMuteRequestOrBuilder extends
    // @@protoc_insertion_point(interface_extends:punishment.DeleteMuteRequest)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>optional string unique_id = 1;</code>
   */
  java.lang.String getUniqueId();
  /**
   * <code>optional string unique_id = 1;</code>
   */
  com.google.protobuf.ByteString
      getUniqueIdBytes();
}
