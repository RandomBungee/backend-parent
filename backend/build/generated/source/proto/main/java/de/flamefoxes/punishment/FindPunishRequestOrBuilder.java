// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: punishment/punish_service.proto

package de.flamefoxes.punishment;

public interface FindPunishRequestOrBuilder extends
    // @@protoc_insertion_point(interface_extends:punishment.FindPunishRequest)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>optional string unique_id = 1;</code>
   */
  java.lang.String getUniqueId();
  /**
   * <code>optional string unique_id = 1;</code>
   */
  com.google.protobuf.ByteString
      getUniqueIdBytes();
}
