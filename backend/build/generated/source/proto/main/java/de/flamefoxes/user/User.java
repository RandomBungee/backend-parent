// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: user/user.proto

package de.flamefoxes.user;

/**
 * Protobuf type {@code user.User}
 */
public  final class User extends
    com.google.protobuf.GeneratedMessage implements
    // @@protoc_insertion_point(message_implements:user.User)
    UserOrBuilder {
  // Use User.newBuilder() to construct.
  private User(com.google.protobuf.GeneratedMessage.Builder builder) {
    super(builder);
  }
  private User() {
    uniqueId_ = "";
    rank_ = "";
    coins_ = 0L;
    wins_ = 0L;
    banPoints_ = 0L;
    banHistory_ = com.google.protobuf.LazyStringArrayList.EMPTY;
    teamspeakUnique_ = "";
    discordTag_ = "";
    isBanned_ = false;
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private User(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry) {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 10: {
            com.google.protobuf.ByteString bs = input.readBytes();

            uniqueId_ = bs;
            break;
          }
          case 18: {
            com.google.protobuf.ByteString bs = input.readBytes();

            rank_ = bs;
            break;
          }
          case 24: {

            coins_ = input.readInt64();
            break;
          }
          case 32: {

            wins_ = input.readInt64();
            break;
          }
          case 40: {

            banPoints_ = input.readInt64();
            break;
          }
          case 50: {
            com.google.protobuf.ByteString bs = input.readBytes();
            if (!((mutable_bitField0_ & 0x00000020) == 0x00000020)) {
              banHistory_ = new com.google.protobuf.LazyStringArrayList();
              mutable_bitField0_ |= 0x00000020;
            }
            banHistory_.add(bs);
            break;
          }
          case 58: {
            com.google.protobuf.ByteString bs = input.readBytes();

            teamspeakUnique_ = bs;
            break;
          }
          case 66: {
            com.google.protobuf.ByteString bs = input.readBytes();

            discordTag_ = bs;
            break;
          }
          case 72: {

            isBanned_ = input.readBool();
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw new RuntimeException(e.setUnfinishedMessage(this));
    } catch (java.io.IOException e) {
      throw new RuntimeException(
          new com.google.protobuf.InvalidProtocolBufferException(
              e.getMessage()).setUnfinishedMessage(this));
    } finally {
      if (((mutable_bitField0_ & 0x00000020) == 0x00000020)) {
        banHistory_ = banHistory_.getUnmodifiableView();
      }
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return de.flamefoxes.user.UserOuterClass.internal_static_user_User_descriptor;
  }

  protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return de.flamefoxes.user.UserOuterClass.internal_static_user_User_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            de.flamefoxes.user.User.class, de.flamefoxes.user.User.Builder.class);
  }

  private int bitField0_;
  public static final int UNIQUE_ID_FIELD_NUMBER = 1;
  private volatile java.lang.Object uniqueId_;
  /**
   * <code>optional string unique_id = 1;</code>
   */
  public java.lang.String getUniqueId() {
    java.lang.Object ref = uniqueId_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      if (bs.isValidUtf8()) {
        uniqueId_ = s;
      }
      return s;
    }
  }
  /**
   * <code>optional string unique_id = 1;</code>
   */
  public com.google.protobuf.ByteString
      getUniqueIdBytes() {
    java.lang.Object ref = uniqueId_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      uniqueId_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int RANK_FIELD_NUMBER = 2;
  private volatile java.lang.Object rank_;
  /**
   * <code>optional string rank = 2;</code>
   */
  public java.lang.String getRank() {
    java.lang.Object ref = rank_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      if (bs.isValidUtf8()) {
        rank_ = s;
      }
      return s;
    }
  }
  /**
   * <code>optional string rank = 2;</code>
   */
  public com.google.protobuf.ByteString
      getRankBytes() {
    java.lang.Object ref = rank_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      rank_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int COINS_FIELD_NUMBER = 3;
  private long coins_;
  /**
   * <code>optional int64 coins = 3;</code>
   */
  public long getCoins() {
    return coins_;
  }

  public static final int WINS_FIELD_NUMBER = 4;
  private long wins_;
  /**
   * <code>optional int64 wins = 4;</code>
   */
  public long getWins() {
    return wins_;
  }

  public static final int BAN_POINTS_FIELD_NUMBER = 5;
  private long banPoints_;
  /**
   * <code>optional int64 ban_points = 5;</code>
   */
  public long getBanPoints() {
    return banPoints_;
  }

  public static final int BAN_HISTORY_FIELD_NUMBER = 6;
  private com.google.protobuf.LazyStringList banHistory_;
  /**
   * <code>repeated string ban_history = 6;</code>
   */
  public com.google.protobuf.ProtocolStringList
      getBanHistoryList() {
    return banHistory_;
  }
  /**
   * <code>repeated string ban_history = 6;</code>
   */
  public int getBanHistoryCount() {
    return banHistory_.size();
  }
  /**
   * <code>repeated string ban_history = 6;</code>
   */
  public java.lang.String getBanHistory(int index) {
    return banHistory_.get(index);
  }
  /**
   * <code>repeated string ban_history = 6;</code>
   */
  public com.google.protobuf.ByteString
      getBanHistoryBytes(int index) {
    return banHistory_.getByteString(index);
  }

  public static final int TEAMSPEAK_UNIQUE_FIELD_NUMBER = 7;
  private volatile java.lang.Object teamspeakUnique_;
  /**
   * <code>optional string teamspeak_unique = 7;</code>
   */
  public java.lang.String getTeamspeakUnique() {
    java.lang.Object ref = teamspeakUnique_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      if (bs.isValidUtf8()) {
        teamspeakUnique_ = s;
      }
      return s;
    }
  }
  /**
   * <code>optional string teamspeak_unique = 7;</code>
   */
  public com.google.protobuf.ByteString
      getTeamspeakUniqueBytes() {
    java.lang.Object ref = teamspeakUnique_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      teamspeakUnique_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int DISCORD_TAG_FIELD_NUMBER = 8;
  private volatile java.lang.Object discordTag_;
  /**
   * <code>optional string discord_tag = 8;</code>
   */
  public java.lang.String getDiscordTag() {
    java.lang.Object ref = discordTag_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      if (bs.isValidUtf8()) {
        discordTag_ = s;
      }
      return s;
    }
  }
  /**
   * <code>optional string discord_tag = 8;</code>
   */
  public com.google.protobuf.ByteString
      getDiscordTagBytes() {
    java.lang.Object ref = discordTag_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      discordTag_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int ISBANNED_FIELD_NUMBER = 9;
  private boolean isBanned_;
  /**
   * <code>optional bool isBanned = 9;</code>
   */
  public boolean getIsBanned() {
    return isBanned_;
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (!getUniqueIdBytes().isEmpty()) {
      output.writeBytes(1, getUniqueIdBytes());
    }
    if (!getRankBytes().isEmpty()) {
      output.writeBytes(2, getRankBytes());
    }
    if (coins_ != 0L) {
      output.writeInt64(3, coins_);
    }
    if (wins_ != 0L) {
      output.writeInt64(4, wins_);
    }
    if (banPoints_ != 0L) {
      output.writeInt64(5, banPoints_);
    }
    for (int i = 0; i < banHistory_.size(); i++) {
      output.writeBytes(6, banHistory_.getByteString(i));
    }
    if (!getTeamspeakUniqueBytes().isEmpty()) {
      output.writeBytes(7, getTeamspeakUniqueBytes());
    }
    if (!getDiscordTagBytes().isEmpty()) {
      output.writeBytes(8, getDiscordTagBytes());
    }
    if (isBanned_ != false) {
      output.writeBool(9, isBanned_);
    }
  }

  private int memoizedSerializedSize = -1;
  public int getSerializedSize() {
    int size = memoizedSerializedSize;
    if (size != -1) return size;

    size = 0;
    if (!getUniqueIdBytes().isEmpty()) {
      size += com.google.protobuf.CodedOutputStream
        .computeBytesSize(1, getUniqueIdBytes());
    }
    if (!getRankBytes().isEmpty()) {
      size += com.google.protobuf.CodedOutputStream
        .computeBytesSize(2, getRankBytes());
    }
    if (coins_ != 0L) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt64Size(3, coins_);
    }
    if (wins_ != 0L) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt64Size(4, wins_);
    }
    if (banPoints_ != 0L) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt64Size(5, banPoints_);
    }
    {
      int dataSize = 0;
      for (int i = 0; i < banHistory_.size(); i++) {
        dataSize += com.google.protobuf.CodedOutputStream
          .computeBytesSizeNoTag(banHistory_.getByteString(i));
      }
      size += dataSize;
      size += 1 * getBanHistoryList().size();
    }
    if (!getTeamspeakUniqueBytes().isEmpty()) {
      size += com.google.protobuf.CodedOutputStream
        .computeBytesSize(7, getTeamspeakUniqueBytes());
    }
    if (!getDiscordTagBytes().isEmpty()) {
      size += com.google.protobuf.CodedOutputStream
        .computeBytesSize(8, getDiscordTagBytes());
    }
    if (isBanned_ != false) {
      size += com.google.protobuf.CodedOutputStream
        .computeBoolSize(9, isBanned_);
    }
    memoizedSerializedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  public static de.flamefoxes.user.User parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static de.flamefoxes.user.User parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static de.flamefoxes.user.User parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static de.flamefoxes.user.User parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static de.flamefoxes.user.User parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return PARSER.parseFrom(input);
  }
  public static de.flamefoxes.user.User parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return PARSER.parseFrom(input, extensionRegistry);
  }
  public static de.flamefoxes.user.User parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return PARSER.parseDelimitedFrom(input);
  }
  public static de.flamefoxes.user.User parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return PARSER.parseDelimitedFrom(input, extensionRegistry);
  }
  public static de.flamefoxes.user.User parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return PARSER.parseFrom(input);
  }
  public static de.flamefoxes.user.User parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return PARSER.parseFrom(input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(de.flamefoxes.user.User prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessage.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code user.User}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessage.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:user.User)
      de.flamefoxes.user.UserOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return de.flamefoxes.user.UserOuterClass.internal_static_user_User_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return de.flamefoxes.user.UserOuterClass.internal_static_user_User_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              de.flamefoxes.user.User.class, de.flamefoxes.user.User.Builder.class);
    }

    // Construct using de.flamefoxes.user.User.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      uniqueId_ = "";

      rank_ = "";

      coins_ = 0L;

      wins_ = 0L;

      banPoints_ = 0L;

      banHistory_ = com.google.protobuf.LazyStringArrayList.EMPTY;
      bitField0_ = (bitField0_ & ~0x00000020);
      teamspeakUnique_ = "";

      discordTag_ = "";

      isBanned_ = false;

      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return de.flamefoxes.user.UserOuterClass.internal_static_user_User_descriptor;
    }

    public de.flamefoxes.user.User getDefaultInstanceForType() {
      return de.flamefoxes.user.User.getDefaultInstance();
    }

    public de.flamefoxes.user.User build() {
      de.flamefoxes.user.User result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public de.flamefoxes.user.User buildPartial() {
      de.flamefoxes.user.User result = new de.flamefoxes.user.User(this);
      int from_bitField0_ = bitField0_;
      int to_bitField0_ = 0;
      result.uniqueId_ = uniqueId_;
      result.rank_ = rank_;
      result.coins_ = coins_;
      result.wins_ = wins_;
      result.banPoints_ = banPoints_;
      if (((bitField0_ & 0x00000020) == 0x00000020)) {
        banHistory_ = banHistory_.getUnmodifiableView();
        bitField0_ = (bitField0_ & ~0x00000020);
      }
      result.banHistory_ = banHistory_;
      result.teamspeakUnique_ = teamspeakUnique_;
      result.discordTag_ = discordTag_;
      result.isBanned_ = isBanned_;
      result.bitField0_ = to_bitField0_;
      onBuilt();
      return result;
    }

    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof de.flamefoxes.user.User) {
        return mergeFrom((de.flamefoxes.user.User)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(de.flamefoxes.user.User other) {
      if (other == de.flamefoxes.user.User.getDefaultInstance()) return this;
      if (!other.getUniqueId().isEmpty()) {
        uniqueId_ = other.uniqueId_;
        onChanged();
      }
      if (!other.getRank().isEmpty()) {
        rank_ = other.rank_;
        onChanged();
      }
      if (other.getCoins() != 0L) {
        setCoins(other.getCoins());
      }
      if (other.getWins() != 0L) {
        setWins(other.getWins());
      }
      if (other.getBanPoints() != 0L) {
        setBanPoints(other.getBanPoints());
      }
      if (!other.banHistory_.isEmpty()) {
        if (banHistory_.isEmpty()) {
          banHistory_ = other.banHistory_;
          bitField0_ = (bitField0_ & ~0x00000020);
        } else {
          ensureBanHistoryIsMutable();
          banHistory_.addAll(other.banHistory_);
        }
        onChanged();
      }
      if (!other.getTeamspeakUnique().isEmpty()) {
        teamspeakUnique_ = other.teamspeakUnique_;
        onChanged();
      }
      if (!other.getDiscordTag().isEmpty()) {
        discordTag_ = other.discordTag_;
        onChanged();
      }
      if (other.getIsBanned() != false) {
        setIsBanned(other.getIsBanned());
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      de.flamefoxes.user.User parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (de.flamefoxes.user.User) e.getUnfinishedMessage();
        throw e;
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int bitField0_;

    private java.lang.Object uniqueId_ = "";
    /**
     * <code>optional string unique_id = 1;</code>
     */
    public java.lang.String getUniqueId() {
      java.lang.Object ref = uniqueId_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          uniqueId_ = s;
        }
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>optional string unique_id = 1;</code>
     */
    public com.google.protobuf.ByteString
        getUniqueIdBytes() {
      java.lang.Object ref = uniqueId_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        uniqueId_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>optional string unique_id = 1;</code>
     */
    public Builder setUniqueId(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      uniqueId_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional string unique_id = 1;</code>
     */
    public Builder clearUniqueId() {
      
      uniqueId_ = getDefaultInstance().getUniqueId();
      onChanged();
      return this;
    }
    /**
     * <code>optional string unique_id = 1;</code>
     */
    public Builder setUniqueIdBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      uniqueId_ = value;
      onChanged();
      return this;
    }

    private java.lang.Object rank_ = "";
    /**
     * <code>optional string rank = 2;</code>
     */
    public java.lang.String getRank() {
      java.lang.Object ref = rank_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          rank_ = s;
        }
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>optional string rank = 2;</code>
     */
    public com.google.protobuf.ByteString
        getRankBytes() {
      java.lang.Object ref = rank_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        rank_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>optional string rank = 2;</code>
     */
    public Builder setRank(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      rank_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional string rank = 2;</code>
     */
    public Builder clearRank() {
      
      rank_ = getDefaultInstance().getRank();
      onChanged();
      return this;
    }
    /**
     * <code>optional string rank = 2;</code>
     */
    public Builder setRankBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      rank_ = value;
      onChanged();
      return this;
    }

    private long coins_ ;
    /**
     * <code>optional int64 coins = 3;</code>
     */
    public long getCoins() {
      return coins_;
    }
    /**
     * <code>optional int64 coins = 3;</code>
     */
    public Builder setCoins(long value) {
      
      coins_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional int64 coins = 3;</code>
     */
    public Builder clearCoins() {
      
      coins_ = 0L;
      onChanged();
      return this;
    }

    private long wins_ ;
    /**
     * <code>optional int64 wins = 4;</code>
     */
    public long getWins() {
      return wins_;
    }
    /**
     * <code>optional int64 wins = 4;</code>
     */
    public Builder setWins(long value) {
      
      wins_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional int64 wins = 4;</code>
     */
    public Builder clearWins() {
      
      wins_ = 0L;
      onChanged();
      return this;
    }

    private long banPoints_ ;
    /**
     * <code>optional int64 ban_points = 5;</code>
     */
    public long getBanPoints() {
      return banPoints_;
    }
    /**
     * <code>optional int64 ban_points = 5;</code>
     */
    public Builder setBanPoints(long value) {
      
      banPoints_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional int64 ban_points = 5;</code>
     */
    public Builder clearBanPoints() {
      
      banPoints_ = 0L;
      onChanged();
      return this;
    }

    private com.google.protobuf.LazyStringList banHistory_ = com.google.protobuf.LazyStringArrayList.EMPTY;
    private void ensureBanHistoryIsMutable() {
      if (!((bitField0_ & 0x00000020) == 0x00000020)) {
        banHistory_ = new com.google.protobuf.LazyStringArrayList(banHistory_);
        bitField0_ |= 0x00000020;
       }
    }
    /**
     * <code>repeated string ban_history = 6;</code>
     */
    public com.google.protobuf.ProtocolStringList
        getBanHistoryList() {
      return banHistory_.getUnmodifiableView();
    }
    /**
     * <code>repeated string ban_history = 6;</code>
     */
    public int getBanHistoryCount() {
      return banHistory_.size();
    }
    /**
     * <code>repeated string ban_history = 6;</code>
     */
    public java.lang.String getBanHistory(int index) {
      return banHistory_.get(index);
    }
    /**
     * <code>repeated string ban_history = 6;</code>
     */
    public com.google.protobuf.ByteString
        getBanHistoryBytes(int index) {
      return banHistory_.getByteString(index);
    }
    /**
     * <code>repeated string ban_history = 6;</code>
     */
    public Builder setBanHistory(
        int index, java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  ensureBanHistoryIsMutable();
      banHistory_.set(index, value);
      onChanged();
      return this;
    }
    /**
     * <code>repeated string ban_history = 6;</code>
     */
    public Builder addBanHistory(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  ensureBanHistoryIsMutable();
      banHistory_.add(value);
      onChanged();
      return this;
    }
    /**
     * <code>repeated string ban_history = 6;</code>
     */
    public Builder addAllBanHistory(
        java.lang.Iterable<java.lang.String> values) {
      ensureBanHistoryIsMutable();
      com.google.protobuf.AbstractMessageLite.Builder.addAll(
          values, banHistory_);
      onChanged();
      return this;
    }
    /**
     * <code>repeated string ban_history = 6;</code>
     */
    public Builder clearBanHistory() {
      banHistory_ = com.google.protobuf.LazyStringArrayList.EMPTY;
      bitField0_ = (bitField0_ & ~0x00000020);
      onChanged();
      return this;
    }
    /**
     * <code>repeated string ban_history = 6;</code>
     */
    public Builder addBanHistoryBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  ensureBanHistoryIsMutable();
      banHistory_.add(value);
      onChanged();
      return this;
    }

    private java.lang.Object teamspeakUnique_ = "";
    /**
     * <code>optional string teamspeak_unique = 7;</code>
     */
    public java.lang.String getTeamspeakUnique() {
      java.lang.Object ref = teamspeakUnique_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          teamspeakUnique_ = s;
        }
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>optional string teamspeak_unique = 7;</code>
     */
    public com.google.protobuf.ByteString
        getTeamspeakUniqueBytes() {
      java.lang.Object ref = teamspeakUnique_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        teamspeakUnique_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>optional string teamspeak_unique = 7;</code>
     */
    public Builder setTeamspeakUnique(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      teamspeakUnique_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional string teamspeak_unique = 7;</code>
     */
    public Builder clearTeamspeakUnique() {
      
      teamspeakUnique_ = getDefaultInstance().getTeamspeakUnique();
      onChanged();
      return this;
    }
    /**
     * <code>optional string teamspeak_unique = 7;</code>
     */
    public Builder setTeamspeakUniqueBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      teamspeakUnique_ = value;
      onChanged();
      return this;
    }

    private java.lang.Object discordTag_ = "";
    /**
     * <code>optional string discord_tag = 8;</code>
     */
    public java.lang.String getDiscordTag() {
      java.lang.Object ref = discordTag_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          discordTag_ = s;
        }
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>optional string discord_tag = 8;</code>
     */
    public com.google.protobuf.ByteString
        getDiscordTagBytes() {
      java.lang.Object ref = discordTag_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        discordTag_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>optional string discord_tag = 8;</code>
     */
    public Builder setDiscordTag(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      discordTag_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional string discord_tag = 8;</code>
     */
    public Builder clearDiscordTag() {
      
      discordTag_ = getDefaultInstance().getDiscordTag();
      onChanged();
      return this;
    }
    /**
     * <code>optional string discord_tag = 8;</code>
     */
    public Builder setDiscordTagBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      discordTag_ = value;
      onChanged();
      return this;
    }

    private boolean isBanned_ ;
    /**
     * <code>optional bool isBanned = 9;</code>
     */
    public boolean getIsBanned() {
      return isBanned_;
    }
    /**
     * <code>optional bool isBanned = 9;</code>
     */
    public Builder setIsBanned(boolean value) {
      
      isBanned_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional bool isBanned = 9;</code>
     */
    public Builder clearIsBanned() {
      
      isBanned_ = false;
      onChanged();
      return this;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:user.User)
  }

  // @@protoc_insertion_point(class_scope:user.User)
  private static final de.flamefoxes.user.User DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new de.flamefoxes.user.User();
  }

  public static de.flamefoxes.user.User getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  public static final com.google.protobuf.Parser<User> PARSER =
      new com.google.protobuf.AbstractParser<User>() {
    public User parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      try {
        return new User(input, extensionRegistry);
      } catch (RuntimeException e) {
        if (e.getCause() instanceof
            com.google.protobuf.InvalidProtocolBufferException) {
          throw (com.google.protobuf.InvalidProtocolBufferException)
              e.getCause();
        }
        throw e;
      }
    }
  };

  @java.lang.Override
  public com.google.protobuf.Parser<User> getParserForType() {
    return PARSER;
  }

  public de.flamefoxes.user.User getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

